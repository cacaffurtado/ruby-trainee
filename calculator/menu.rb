require_relative 'operations'
require_relative '../extra_operations'

module Calculator
  class Menu
    include ExtraOperations

    def initialize
      
      operations = Operations.new

      puts "-------------------------"
      puts " Bem vindo a Calculadora "
      puts "-------------------------"
      puts "Qual operação você gostaria de executar? "
      puts "1- Média Preconceituosa"
      puts "2- Calculadora sem números"
      puts "3- Filtrar filmes"
      puts "0- Encerrar calculadora."

      option = gets.chomp.to_i

      system 'clear'
     
      if option == 1

        puts "-> Digite o JSON com as notas [{aluno: nota}]:"
        grades = gets.chomp
        puts "-> Quais notas devem ser ignoradas [{aluno}]? "
        blacklist = gets.chomp

        media = operations.biased_mean(grades,blacklist)
        puts "-> A média final é #{media}"

      elsif option == 2
        puts "-> Digite uma sequência de números e será informado se eles são ou não divisíveis por 25"

        numbers = gets.chomp

        result = operations.no_integers(numbers)
        puts "#{result}"
        
      elsif option == 3
        puts "Uma pesquisa sobre filmes!" 
        puts "-> Em inglês, digite gênero(s) para filtrar a lista de filmes: "

        genres = gets.chomp

        puts "-> Agora, diga qual o ano de lançamento: "
        
        year = gets.chomp.to_i
        
        movies = operations.filter_films(genres,year)
        puts movies

      elsif option == 0
        puts "-> Calculadora Encerrada"
        exit

      else 
        puts "-> Opção inválida!"

      end

    end
  end
end
